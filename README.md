# Android Architecture Components #

A new collection of libraries that help you design robust, testable, and maintainable apps. Start with classes for managing your UI component lifecycle and handling data persistence.

Read more about
https://developer.android.com/topic/libraries/architecture/index.html

Demo app is used for manupulation item list. It should survive configuration changes, by persisting data into the Room library.

### How do I get set up? ###

* Android Studio 3.0
* Sync gradle dependences
