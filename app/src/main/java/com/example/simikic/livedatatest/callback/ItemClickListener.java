package com.example.simikic.livedatatest.callback;

/**
 * Created by simikic on 10/26/2017.
 */

public interface ItemClickListener {

    void onItemClicked(int position);
}
