package com.example.simikic.livedatatest.di;

import javax.inject.Scope;

/**
 * Created by simikic on 11/16/2017.
 */
@Scope
public @interface ApplicationScope {
}
