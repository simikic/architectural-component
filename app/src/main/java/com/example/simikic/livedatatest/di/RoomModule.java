package com.example.simikic.livedatatest.di;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.persistence.room.Room;

import com.example.simikic.livedatatest.data.Repository;
import com.example.simikic.livedatatest.db.AppDatabase;
import com.example.simikic.livedatatest.db.QuestionDao;
import com.example.simikic.livedatatest.viewmodel.SurveyViewModelFactory;

import dagger.Module;
import dagger.Provides;

/**
 * Created by simikic on 11/14/2017.
 */
@Module
public class RoomModule {

    @Provides
    @ApplicationScope
    AppDatabase provideDatabase(Application application) {
        return Room.databaseBuilder(application, AppDatabase.class, AppDatabase.DATABASE_NAME).build();
    }

    @Provides
    @ApplicationScope
    QuestionDao provideDao(AppDatabase db) {
        return db.questionDao();
    }

    @Provides
    @ApplicationScope
    Repository provideRepository(QuestionDao questionDao) {
        return new Repository(questionDao);
    }

    @Provides
    @ApplicationScope
    ViewModelProvider.Factory provideViewModelFactory(Repository repository) {
        return new SurveyViewModelFactory(repository);
    }
}
