package com.example.simikic.livedatatest.ui.adapter.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.example.simikic.livedatatest.R;
import com.example.simikic.livedatatest.callback.ItemClickListener;
import com.example.simikic.livedatatest.db.entity.SurveyEntity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by simikic on 10/26/2017.
 */

public class QuestionViewHolder extends RecyclerView.ViewHolder {

    private static final String TAG = QuestionViewHolder.class.getSimpleName();

    @BindView(R.id.tv_order)
    TextView mOrder;
    @BindView(R.id.tv_question)
    TextView mQuestion;
    @BindView(R.id.tv_answer)
    TextView mAnswer;

    private Context mContext;
    private ItemClickListener mCallback;

    public QuestionViewHolder(View itemView, ItemClickListener callback) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        mCallback = callback;
        itemView.setOnClickListener(view -> mCallback.onItemClicked(getAdapterPosition()));

        mContext = itemView.getContext();
    }

    public void bind(SurveyEntity question) {
        String questionStr = question.getQuestion();
        String answerStr = question.getAnswer();
        if (TextUtils.isEmpty(questionStr)) {
            questionStr = mContext.getString(R.string.question);
        }
        if (TextUtils.isEmpty(answerStr)) {
            answerStr = mContext.getString(R.string.answer);
        }

        mOrder.setText((getAdapterPosition() + 1) + ".");
        mQuestion.setText(questionStr);
        mAnswer.setText(answerStr);
    }
}