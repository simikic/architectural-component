package com.example.simikic.livedatatest.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.simikic.livedatatest.db.entity.SurveyEntity;

import java.util.List;

/**
 * Created by simikic on 10/26/2017.
 */

@Dao
public interface QuestionDao {

    @Query("SELECT * FROM SurveyEntity")
    LiveData<List<SurveyEntity>> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(SurveyEntity question);

    @Query("UPDATE SurveyEntity SET answer = :answer  WHERE id = :id")
    void update(int id, String answer);
}