package com.example.simikic.livedatatest.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.simikic.livedatatest.R;
import com.example.simikic.livedatatest.ui.fragment.SurveyListFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_main);

        if (savedInstanceState == null) {
            SurveyListFragment fragment = new SurveyListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fl_container, fragment, SurveyListFragment.TAG).commit();
        }
    }
}
