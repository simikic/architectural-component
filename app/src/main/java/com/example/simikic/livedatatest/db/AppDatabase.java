package com.example.simikic.livedatatest.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.simikic.livedatatest.db.entity.SurveyEntity;

/**
 * Created by simikic on 10/26/2017.
 */
@Database(entities = {SurveyEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public static final String DATABASE_NAME = "database";

    public abstract QuestionDao questionDao();
}


