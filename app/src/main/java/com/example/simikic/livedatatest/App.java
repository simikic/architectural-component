package com.example.simikic.livedatatest;

import android.app.Application;

import com.example.simikic.livedatatest.di.ApplicationComponent;
import com.example.simikic.livedatatest.di.ApplicationModule;
import com.example.simikic.livedatatest.di.DaggerApplicationComponent;
import com.example.simikic.livedatatest.di.RoomModule;

/**
 * Created by simikic on 10/26/2017.
 */

public class App extends Application {

    private static App sInstance;
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .roomModule(new RoomModule())
                .build();
    }

    public static App get() {
        return sInstance;
    }

    public ApplicationComponent applicationComponent() {
        return applicationComponent;
    }
}
