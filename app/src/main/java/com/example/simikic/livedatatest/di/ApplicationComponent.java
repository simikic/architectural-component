package com.example.simikic.livedatatest.di;

import com.example.simikic.livedatatest.ui.fragment.SurveyDialogFragment;
import com.example.simikic.livedatatest.ui.fragment.SurveyListFragment;

import dagger.Component;

/**
 * Created by simikic on 11/14/2017.
 */
@ApplicationScope
@Component(modules = {ApplicationModule.class, RoomModule.class})
public interface ApplicationComponent {
    void inject(SurveyListFragment surveyListFragment);

    void inject(SurveyDialogFragment surveyDialogFragment);
}
