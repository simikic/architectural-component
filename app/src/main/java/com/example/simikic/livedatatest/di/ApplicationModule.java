package com.example.simikic.livedatatest.di;

import android.app.Application;

import dagger.Module;
import dagger.Provides;

/**
 * Created by simikic on 11/14/2017.
 */
@Module
public class ApplicationModule {
    private final Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    Application provideApplication() {
        return application;
    }
}
