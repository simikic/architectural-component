package com.example.simikic.livedatatest.ui.fragment;

import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.simikic.livedatatest.App;
import com.example.simikic.livedatatest.R;
import com.example.simikic.livedatatest.common.AppConstants;
import com.example.simikic.livedatatest.db.entity.SurveyEntity;
import com.example.simikic.livedatatest.viewmodel.SurveyViewModel;
import com.example.simikic.livedatatest.viewmodel.SurveyViewModelFactory;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by simikic on 11/14/2017.
 */
public class SurveyDialogFragment extends DialogFragment {

    private static final String TAG = SurveyDialogFragment.class.getSimpleName();

    @BindView(R.id.tv_question)
    TextView mQuestion;

    @BindView(R.id.et_question)
    EditText mEditableQuestion;

    @BindView(R.id.et_answer)
    EditText mAnswer;

    private SurveyEntity mSurvey;
    private SurveyViewModel mViewModel;

    @Inject
    SurveyViewModelFactory mViewModelFactory;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.get().applicationComponent().inject(this);
    }

    public static SurveyDialogFragment newInstance(SurveyEntity surveyEntity) {
        SurveyDialogFragment fragment = new SurveyDialogFragment();
        if (surveyEntity != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(AppConstants.ARG_SURVEY, surveyEntity);
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders
                .of(this, mViewModelFactory)
                .get(SurveyViewModel.class);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.d_f_survey, null);
        ButterKnife.bind(this, dialogView);

        int answerStr, titleStr;
        if (getArguments() != null) {
            mSurvey = getArguments().getParcelable(AppConstants.ARG_SURVEY);
            mQuestion.setText(mSurvey.getQuestion());
            mEditableQuestion.setVisibility(View.GONE);
            mAnswer.setText(mSurvey.getAnswer());
            answerStr = R.string.answer;
            titleStr = R.string.surrey_question;
        } else {
            // create new question
            mQuestion.setVisibility(View.GONE);
            mAnswer.setVisibility(View.GONE);
            answerStr = R.string.create;
            titleStr = R.string.new_question;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        return builder
                .setTitle(titleStr)
                .setView(dialogView)
                .setPositiveButton(answerStr, (dialogInterface, i) -> {
                    if (mSurvey == null) {
                        mSurvey = new SurveyEntity(0, mEditableQuestion.getText().toString(), "");
                        mViewModel.addItem(mSurvey);
                    } else {
                        SurveyEntity updateSurvey = new SurveyEntity(mSurvey);
                        updateSurvey.setAnswer(mAnswer.getText().toString());
                        mViewModel.updateItem(updateSurvey);
                    }

                }).setNegativeButton(R.string.cancel, null)
                .create();
    }
}
