package com.example.simikic.livedatatest.ui.adapter;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.simikic.livedatatest.R;
import com.example.simikic.livedatatest.callback.ItemClickListener;
import com.example.simikic.livedatatest.db.entity.SurveyEntity;
import com.example.simikic.livedatatest.ui.adapter.viewholder.QuestionViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by simikic on 10/26/2017.
 */

public class QuestionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = QuestionAdapter.class.getSimpleName();

    private ItemClickListener mCallback;
    private List<SurveyEntity> mData;

    public QuestionAdapter(ItemClickListener callback) {
        this.mCallback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.li_question, parent, false);
        return new QuestionViewHolder(v, mCallback);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        QuestionViewHolder qvh = (QuestionViewHolder) holder;
        qvh.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    public SurveyEntity getItem(int position) {
        if (position >= 0 && position < mData.size()) {
            return mData.get(position);
        }
        return null;
    }

    public void setQuestionList(final List<SurveyEntity> questions) {
        if (mData == null) {
            mData = new ArrayList<>(questions);
            notifyItemRangeInserted(0, mData.size());
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return mData.size();
                }

                @Override
                public int getNewListSize() {
                    return questions.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return mData.get(oldItemPosition).getId() == questions.get(newItemPosition).getId();
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    SurveyEntity oldQuestion = mData.get(oldItemPosition);
                    SurveyEntity newQuestion = questions.get(newItemPosition);
                    return newQuestion.equals(oldQuestion);
                }
            });
            mData = questions;
            result.dispatchUpdatesTo(this);
        }
    }
}
