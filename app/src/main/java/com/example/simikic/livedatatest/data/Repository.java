package com.example.simikic.livedatatest.data;

import android.arch.lifecycle.LiveData;

import com.example.simikic.livedatatest.db.QuestionDao;
import com.example.simikic.livedatatest.db.entity.SurveyEntity;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Inject;

/**
 * Created by simikic on 10/26/2017.
 */

public class Repository {

    private final Executor executor = Executors.newFixedThreadPool(2);
    private final QuestionDao mQuestionDao;

    @Inject
    public Repository(QuestionDao questionDao) {
        this.mQuestionDao = questionDao;
    }

    public LiveData<List<SurveyEntity>> getData() {
        return mQuestionDao.getAll();
    }

    public void add(SurveyEntity question) {
        executor.execute(() -> mQuestionDao.insert(question));
    }

    public void update(SurveyEntity question) {
        executor.execute(() -> mQuestionDao.update(question.getId(), question.getAnswer()));
    }
}
