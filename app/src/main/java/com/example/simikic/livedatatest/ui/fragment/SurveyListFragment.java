package com.example.simikic.livedatatest.ui.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.simikic.livedatatest.App;
import com.example.simikic.livedatatest.R;
import com.example.simikic.livedatatest.callback.ItemClickListener;
import com.example.simikic.livedatatest.ui.adapter.QuestionAdapter;
import com.example.simikic.livedatatest.viewmodel.SurveyViewModel;
import com.example.simikic.livedatatest.viewmodel.SurveyViewModelFactory;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by simikic on 10/26/2017.
 */
public class SurveyListFragment extends Fragment implements ItemClickListener {

    public static final String TAG = SurveyListFragment.class.getSimpleName();

    @BindView(R.id.rv_survey_list)
    RecyclerView mSurveyList;

    @Inject
    SurveyViewModelFactory mViewModelFactory;
    
    private QuestionAdapter mSurveyAdapter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        App.get().applicationComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_survey, null);
        ButterKnife.bind(this, view);

        mSurveyAdapter = new QuestionAdapter(this);
        mSurveyList.setAdapter(mSurveyAdapter);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SurveyViewModel viewModel = ViewModelProviders
                .of(this, mViewModelFactory)
                .get(SurveyViewModel.class);

        // Update the list when the data changes
        viewModel.getQuestionList().observe(this, questions -> {
            if (questions != null) {
                mSurveyAdapter.setQuestionList(questions);
            }
        });
    }

    @Override
    public void onItemClicked(int position) {
        SurveyDialogFragment sdf = SurveyDialogFragment.newInstance(mSurveyAdapter.getItem(position));
        sdf.show(getFragmentManager(), "answerSurvey");
    }

    @OnClick(R.id.f_add)
    public void createNewClicked() {
        SurveyDialogFragment sdf = SurveyDialogFragment.newInstance(null);
        sdf.show(getFragmentManager(), "answerSurvey");
    }
}
