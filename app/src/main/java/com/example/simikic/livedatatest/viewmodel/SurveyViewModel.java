package com.example.simikic.livedatatest.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.simikic.livedatatest.data.Repository;
import com.example.simikic.livedatatest.db.entity.SurveyEntity;

import java.util.List;

/**
 * Created by simikic on 10/26/2017.
 */

public class SurveyViewModel extends ViewModel {

    private Repository repository;

    public SurveyViewModel(Repository repository) {
        this.repository = repository;
    }

    public LiveData<List<SurveyEntity>> getQuestionList() {
        return repository.getData();
    }

    public void addItem(SurveyEntity newItem) {
        repository.add(newItem);
    }

    public void updateItem(SurveyEntity survey) {
        repository.update(survey);
    }

}