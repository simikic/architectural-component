package com.example.simikic.livedatatest.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.example.simikic.livedatatest.data.Repository;

import javax.inject.Inject;

/**
 * Created by simikic on 11/14/2017.
 */
public class SurveyViewModelFactory implements ViewModelProvider.Factory {

    private Repository repository;

    @Inject
    public SurveyViewModelFactory(Repository repository) {
        this.repository = repository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SurveyViewModel.class))
            return (T) new SurveyViewModel(repository);
        else {
            throw new IllegalArgumentException("ViewModel Not Found");
        }
    }
}
