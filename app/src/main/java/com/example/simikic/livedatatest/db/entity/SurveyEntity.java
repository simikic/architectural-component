package com.example.simikic.livedatatest.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by simikic on 10/26/2017.
 */
@Entity
public class SurveyEntity implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "type")
    private int mType;
    @ColumnInfo(name = "question")
    private String mQuestion;
    @ColumnInfo(name = "answer")
    private String mAnswer;

    public SurveyEntity(int type, String question, String answer) {
        this.mType = type;
        this.mQuestion = question;
        this.mAnswer = answer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return mType;
    }

    public void setType(int type) {
        mType = type;
    }

    public String getQuestion() {
        return mQuestion;
    }

    public void setQuestion(String question) {
        mQuestion = question;
    }

    public String getAnswer() {
        return mAnswer;
    }

    public void setAnswer(String newAnswer) {
        mAnswer = newAnswer;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof SurveyEntity)) {
            return false;
        }
        SurveyEntity theirQuestion = (SurveyEntity) obj;
        if (theirQuestion.mType != this.mType) {
            return false;
        }

        if (!theirQuestion.mQuestion.equals(this.mQuestion)) {
            return false;
        }

        if (!theirQuestion.mAnswer.equals(this.mAnswer)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return id + " " + mQuestion + " " + mAnswer;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.mType);
        dest.writeString(this.mQuestion);
        dest.writeString(this.mAnswer);
    }

    public SurveyEntity(Parcel in) {
        this.id = in.readInt();
        this.mType = in.readInt();
        this.mQuestion = in.readString();
        this.mAnswer = in.readString();
    }

    public static final Parcelable.Creator<SurveyEntity> CREATOR = new Parcelable.Creator<SurveyEntity>() {
        @Override
        public SurveyEntity createFromParcel(Parcel source) {
            return new SurveyEntity(source);
        }

        @Override
        public SurveyEntity[] newArray(int size) {
            return new SurveyEntity[size];
        }
    };

    // Copy constructor
    public SurveyEntity(SurveyEntity fromThisObject) {
        this.id = fromThisObject.getId();
        this.mType = fromThisObject.getType();
        this.mQuestion = fromThisObject.getQuestion();
        this.mAnswer = fromThisObject.getAnswer();
    }
}
